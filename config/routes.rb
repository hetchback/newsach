Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # mount Commontator::Engine => '/commontator'
  resources :articles
  # devise_for :admin_users, { class_name: 'User' }.merge(ActiveAdmin::Devise.config)
  # ActiveAdmin.routes(self)
  # devise_for :users # , controllers: { sessions: 'users/sessions' }
  # devise_for :users, controllers: { sessions: 'users/sessions' }
  devise_for :users, controllers: {
    registrations: 'users/registrations',
    passwords: 'users/passwords',
    sessions: 'users/sessions',
    confirmations: 'users/confirmations',
    omniauth_callbacks: 'users/omniauth_callbacks'
  }
  # resources :users
  root 'articles#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
