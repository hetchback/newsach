class Rating < ApplicationRecord
    belongs_to :user
    belongs_to :article
    validates :stars, numericality: {
        only_integer: true,
        greater_than_or_equal_to: 1,
        less_than_or_equal_to: 5
    }
    validates :user_id, uniqueness: { scope: :article_id }
end
