class Article < ApplicationRecord
  # acts_as_commontable dependent: :destroy
  belongs_to :author, class_name: 'User', foreign_key: 'user_id'
  has_many :ratings
  has_many :appreciators, through: :ratings, source: :user
  # has_many :comments, as: :commentable
  has_and_belongs_to_many :tags

  validates :author, presence: true
  validates_associated :author
  validates_length_of :description, maximum: 256

  def rating
    ratings.average(:stars)
  end

  rails_admin do
    configure :author do
      label 'Author: '
    end
  end
end
