class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  ROLES = %w(admin writer base banned)
  devise :database_authenticatable, :confirmable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, 
         :omniauthable, omniauth_providers: [:twitter, :facebook, :vkontakte]
  # acts_as_commontator  
  has_many :articles
  has_many :ratings
  has_many :likes
  # has_many :comments
  has_many :estimated_articles, through: :ratings, source: :article

  def all_likes
    Like.where(user_id: id).count
  end

  def role?(base_role)
    ROLES.index(base_role.to_s) <= ROLES.index(role)
  end

  def self.from_omniauth(auth)
    logger.debug auth
    if auth.info.email
      user = User.find_by(email: auth.info.email) 
      if user and user.confirmed?
        user.provider = auth.provider
        user.uid = auth.uid
        return user
      end
    end
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email || "#{}@tmp.com"
      user.password = Devise.friendly_token[0,20]
      # user.name = auth.info.name   # assuming the user model has a name
      # user.image = auth.info.image # assuming the user model has an image
      user.skip_confirmation!
    end
  end

  # def self.new_with_session(params, session)
  #   super.tap do |user|
  #     if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
  #       user.email = data["email"] if user.email.blank?
  #     end
  #   end
  # end
end
