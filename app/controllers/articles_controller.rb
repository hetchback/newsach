class ArticlesController < InheritedResources::Base
  load_and_authorize_resource
  before_action :authenticate_user!, except: [:show, :index]

  def index
    @articles = Article.where(published: true).order('published_at DESC')
  end

  def new
    if current_user.role?(:admin) && params[:author]
      @article.author = User.where(id: params[:author]).first
    else
      @article.author = current_user
    end
  end

  def create
    if @article.published
      @article.published_at = Time.now
    end
    if @article.save
      flash[:success] = "Post was created successfully"
      redirect_to @article
    else
      render :new
    end
  end

  def update
    if @article.published
      @article.published_at = Time.now
    end
    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def show
    commontator_thread_show(@article)
  end

  private
    def article_params
      params.require(:article).permit(:title, :description, :text, :author, :published, :published_at, :lock_version)
    end
end
