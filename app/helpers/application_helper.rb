module ApplicationHelper
    def markdown(text)
        options = {
          filter_html:     true,
          hard_wrap:       true,
          link_attributes: { rel: 'nofollow', target: "_blank" },
          space_after_headers: true,
          fenced_code_blocks: true,
          prettify: true
        }
    
        extensions = {
            autolink: true,
            no_intra_emphasis: true,
            fenced_code_blocks: true,
            lax_html_blocks: true,
            strikethrough: true,
            superscript: true,
            space_after_headers: true
        }
    
        renderer = Redcarpet::Render::HTML.new(options)
        markdown = Redcarpet::Markdown.new(renderer, extensions)
    
        markdown.render(text.to_s).html_safe
    end

    def render_404
        raise ActionController::RoutingError.new('Not Found')
    end
end
