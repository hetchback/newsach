# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Rails.env.development?
    User.create!(email: 'admin@admin.com', password: '111111', password_confirmation: '111111', confirmed_at: Time.now, role: 'admin')
    User.create!(email: 'writer@writer.com', password: '111111', password_confirmation: '111111', confirmed_at: Time.now, role: 'writer')  
    User.create!(email: 'base@base.com', password: '111111', password_confirmation: '111111', confirmed_at: Time.now) 
    User.create!(email: 'banned@banned.com', password: '111111', password_confirmation: '111111', confirmed_at: Time.now, role: 'banned') 

    10.times do
        Article.create title: 'Some admin title', description: 'Some description', text: '# Some headling\nSome text', user_id: 1, published: true, published_at: Time.now
        Article.create title: 'Some writer title', description: 'Some description', text: '# Some headling\nSome text', user_id: 2, published: true, published_at: Time.now  
    end
end